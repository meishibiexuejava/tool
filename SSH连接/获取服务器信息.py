import paramiko
import re


# 定义一个函数用于连接远程服务器
def connect(ip, password):
    try:
        # 创建ssh客户端
        ssh = paramiko.SSHClient()
        # 允许连接不在know_hosts文件中的主机
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        # 连接服务器
        ssh.connect(hostname=ip, port=22, username='root', password=password)
        # 返回ssh客户端
        return ssh
    except Exception as e:
        print(e)
        return None


# 获取磁盘信息
def get_disk_info(ssh):
    # 执行命令
    stdin, stdout, stderr = ssh.exec_command("df -h")
    # 获取命令输出结果
    result = stdout.read().decode()
    # 处理输出结果
    disk_info = []
    for line in result.split('\n')[1:-1]:
        items = re.findall('\S+', line)
        disk = {
            'filesystem': items[0],
            'size': items[1],
            'used': items[2],
            'free': items[3],
            'percent_used': items[4],
            'mount_point': items[5]
        }
        disk_info.append(disk)
    # 返回磁盘信息
    return disk_info


# 获取目录信息
def get_dir_info(ssh):
    # 目录列表
    dirs = ['/var', '/usr', '/tmp']
    # 获取目录信息
    dir_info = []
    for dir in dirs:
        # 执行命令
        stdin, stdout, stderr = ssh.exec_command(f"du -sh {dir}")
        # 获取命令输出结果
        result = stdout.read().decode()
        # 处理输出结果
        items = re.findall('\S+', result)
        dir = {
            'dir': dir,
            'size': items[0],
            'used': items[0],
            'free': '0B',
            'percent_used': '100%',
        }
        dir_info.append(dir)
    # 返回目录信息
    return dir_info


# 获取内存信息
def get_mem_info(ssh):
    # 执行命令
    stdin, stdout, stderr = ssh.exec_command("free -m")
    # 获取命令输出结果
    result = stdout.read().decode()
    # 处理输出结果，排除掉包含表头的行
    lines = result.split('\n')[1:]
    items = re.findall('\S+', lines[0])
    mem_info = {
        'total': items[1],
        'used': items[2],
        'free': items[3],
        'percent_used': "{:.2f}%".format(int(items[2]) / int(items[1]) * 100),
    }
    # 返回内存信息
    return mem_info


# 获取线程数和cpu使用量
def get_thread_and_cpu_info(ssh):
    # 执行命令
    stdin, stdout, stderr = ssh.exec_command("top -b -n 1")
    # 获取命令输出结果
    result = stdout.read().decode()
    # 处理输出结果
    thread_num = re.findall('Tasks:\s+(\d+)\s', result)[0]
    cpu_percent = re.findall('Cpu\(s\):\s+(\d+\.\d+)\s', result)[0]
    # 返回线程数和cpu使用量
    return thread_num, cpu_percent


# 获取用户信息
def get_user_info(ssh):
    # 执行命令
    stdin, stdout, stderr = ssh.exec_command("cat /etc/passwd")
    # 获取命令输出结果
    result = stdout.read().decode()
    # 处理输出结果
    users = re.findall('(\w+):x:\d+:\d+', result)
    user_info = {
        'user_num': len(users),
        'users': users
    }
    # 返回用户信息
    return user_info


# 主函数
def main():
    # 获取用户输入
    ip = input("请输入服务器IP地址：")
    password = input("请输入服务器密码：")
    # 连接远程服务器
    ssh = connect(ip, password)
    # 获取磁盘信息
    disk_info = get_disk_info(ssh)
    print("磁盘信息：")
    for disk in disk_info:
        print(f"文件系统：{disk['filesystem']}，总空间：{disk['size']}，已用空间：{disk['used']}，剩余空间：{disk['free']}，已用空间百分比：{disk['percent_used']}，挂载点：{disk['mount_point']}")
    # 获取目录信息
    dir_info = get_dir_info(ssh)
    print("目录信息：")
    for dir in dir_info:
        print(f"目录：{dir['dir']}，总空间：{dir['size']}，已用空间：{dir['used']}，剩余空间：{dir['free']}，已用空间百分比：{dir['percent_used']}")
    # 获取内存信息
    mem_info = get_mem_info(ssh)
    print(f"内存总量：{mem_info['total']}，已使用内存：{mem_info['used']}，剩余内存：{mem_info['free']}，已使用内存百分比：{mem_info['percent_used']}")
    # 获取线程数和cpu使用量
    thread_num, cpu_percent = get_thread_and_cpu_info(ssh)
    print(f"线程数：{thread_num}，CPU使用量：{cpu_percent}%")
    # 获取用户信息
    user_info = get_user_info(ssh)
    print(f"用户数量：{user_info['user_num']}，用户名：{', '.join(user_info['users'])}")
    # 关闭ssh连接
    ssh.close()


if __name__ == '__main__':
    main()