import psutil

ip = input("请输入IP地址：")
password = input("请输入密码：")

# 获取CPU使用率
cpu_percent = psutil.cpu_percent()

# 获取磁盘信息
disk_info = psutil.disk_usage('/')
disk_total = disk_info.total // (1024 * 1024)
disk_free = disk_info.free // (1024 * 1024)
disk_used = disk_info.used // (1024 * 1024)

# 获取内存信息
mem_info = psutil.virtual_memory()
mem_total = mem_info.total // 1024
mem_free = mem_info.free // 1024
mem_used = mem_info.used // 1024

# 获取线程数
thread_count = psutil.cpu_count()

print(f"CPU使用率：{cpu_percent}%")
print(f"磁盘总空间：{disk_total}MB")
print(f"磁盘剩余空间：{disk_free}MB")
print(f"磁盘已使用空间：{disk_used}MB")
print(f"内存总量：{mem_total}KB")
print(f"内存剩余空间：{mem_free}KB")
print(f"内存已使用空间：{mem_used}KB")
print(f"线程数：{thread_count}")
