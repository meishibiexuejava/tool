import os
import re
import random

for file in os.listdir("图"):
    # 判断是否是文件
    if os.path.isfile(os.path.join("图", file)):
        # 用正则匹配，去掉不需要的词
        newName = re.sub("@.*$", "", file)
        print(newName)
        # 设置新文件名
        newFilename = file.replace(file, ''.join(str(random.randint(0, 9)) for j in range(10))+newName)
        # 重命名
        os.rename(os.path.join("图", file), os.path.join("图", newFilename))
print("文件名已统一修改成功")