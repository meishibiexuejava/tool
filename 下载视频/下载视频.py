import os
import requests
import random

video_url = "https://v3-web.douyinvod.com/2d7db40342ceebb60cf3ed23d97b5e99/65ec537d/video/tos/cn/tos-cn-ve-15/oMTZfs2DC9gp7EtlAoAR9Lq86bfwkMADAn8BgG/"  # 视频URL

# 生成随机的输出文件名，以避免文件覆盖，同时确保文件名唯一。
output_filename = os.path.join('素材', ''.join(str(random.randint(0, 9)) for j in range(16))+".mp4")

# 发起HTTP请求下载视频。
response = requests.get(video_url, stream=True)
if response.status_code == 200:
    # 如果请求成功，将视频内容写入本地文件。
    with open(output_filename, 'wb') as f:
        for chunk in response.iter_content(chunk_size=1024):
            if chunk:  # 忽略空块
                f.write(chunk)
    # 下载成功后，打印消息。
    print(f"视频下载成功 {output_filename}")
else:
    # 如果请求失败，打印错误消息。
    print(f"视频下载失败. Status code: {response.status_code}")